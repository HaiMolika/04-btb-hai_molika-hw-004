
public class MainClass {
    public static void main(String[] arg){
        MyCollection<Integer> list = new MyCollection<>();
        try{
            list.addItem(12);
            list.addItem(23);
            list.addItem(34);
            list.addItem(null);
        }catch (Exception e){
            System.out.println(e);
        }
        System.out.println("\nlist all from inputted : ");
        for (int i = 0 ; i<list.size(); i++){
                System.out.println(list.getItem(i));
        }
    }
}
