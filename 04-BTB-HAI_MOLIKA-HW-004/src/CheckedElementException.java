public class CheckedElementException extends Exception {
    CheckedElementException(){
        super();
    }
    CheckedElementException(String message){
        super(message);
    }
}
