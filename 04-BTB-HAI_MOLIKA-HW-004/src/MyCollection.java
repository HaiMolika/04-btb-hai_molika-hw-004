public class MyCollection <T>{

    T []list = null;

    // to each element must be [not null], [no duplication]
    void addItem(T item) throws CheckedElementException {
        if (item == null){
            throw new CheckedElementException("Null Value.");
        }
        Object[] subList;
        if (list == null){
            subList = new Object[1];
        }
        else {
            for (T i : list){
                if (item == i){
                    throw new CheckedElementException("Duplicated Value [ " + item +" ]");
                }
            }
            subList = new Object[list.length + 1];
            for (int i = 0; i < list.length; i++) {
                subList[i] = list[i];
            }
        }
        subList[subList.length-1] = item;
        list = (T[]) subList;
    }
    T getItem(int i){
        return list[i];
    }
    int size(){
        if (list == null){
            System.out.println("No inputted element yet.");
            return 0;
        }
        else{
            return list.length;
        }
    }
}
